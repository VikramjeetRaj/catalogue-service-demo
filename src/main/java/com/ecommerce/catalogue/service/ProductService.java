package com.ecommerce.catalogue.service;

import java.util.List;

import com.ecommerce.catalogue.model.Product;

public interface ProductService {
	Product findById(Long id);

	Product save(Product product);

	Product update(Product product, Long id);

	List<Product> findAllProductGroupBy(String field, int page, int pageSize, int sortOrder);

	Integer getNoOfProductBySeller(Long sellerId);
}
