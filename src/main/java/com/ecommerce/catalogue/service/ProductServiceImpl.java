package com.ecommerce.catalogue.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ecommerce.catalogue.model.Product;
import com.ecommerce.catalogue.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public Product findById(Long id) {
		return this.productRepository.findById(id).orElseGet(null);
	}

	@Override
	public Product save(Product product) {
		return this.productRepository.save(product);
	}

	public Product update(Product inputProduct, Long id) {
		return productRepository.findById(id).map(product -> {
			product.setBrand(inputProduct.getBrand());
			product.setCategories(inputProduct.getCategories());
			product.setColor(inputProduct.getColor());
			product.setSeller(inputProduct.getSeller());
			product.setSize(inputProduct.getSize());
			product.setPrice(inputProduct.getPrice());
			product.setName(inputProduct.getName());
			return productRepository.save(product);
		}).orElseGet(() -> {
			inputProduct.setId(id);
			return productRepository.save(inputProduct);
		});

	}

	public List<Product> findAllProductGroupBy(String field, int page, int pageSize, int sortOrder) {
		Sort.Direction direction;

		if (sortOrder == 0) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}
		Page<Product> productPage = this.productRepository.findAll(PageRequest.of(page, pageSize, direction, field));
		return productPage.toList();
	}

	public Integer getNoOfProductBySeller(Long sellerId) {
		return this.productRepository.countBySellerId(sellerId);
	}

}
