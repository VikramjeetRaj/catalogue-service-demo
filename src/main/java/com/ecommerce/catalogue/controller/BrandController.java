package com.ecommerce.catalogue.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.catalogue.model.Brand;
import com.ecommerce.catalogue.service.BrandService;

@RestController
@RequestMapping("api/v1")
public class BrandController {
	@Autowired
	private BrandService brandService;

	@GetMapping("brand")
	public @ResponseBody List<Brand> getAllBrand(@RequestParam Integer page, @RequestParam Integer pageSize,
			@RequestParam Integer sortOrder) {
		return this.brandService.findAll(page, pageSize, sortOrder);
	}

	@CacheEvict(value = "products", allEntries = true)
	@PostMapping("brand")
	public @ResponseBody Brand newBrand(@Valid @RequestBody Brand brand) {
		return this.brandService.save(brand);
	}

	@CacheEvict(value = "products", allEntries = true)
	@PutMapping("/brand/{id}")
	public @ResponseBody Brand replaceBrand(@Valid @RequestBody Brand brand, @PathVariable Long id) {
		return this.brandService.update(brand, id);
	}
}
