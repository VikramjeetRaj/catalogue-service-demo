package com.ecommerce.catalogue.exception;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class,
			EntityNotFoundException.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		return handleExceptionInternal(ex, request);
	}

	private ResponseEntity<Object> handleExceptionInternal(RuntimeException ex, WebRequest request) {
		HttpStatus errorStatus = null;
		String message = ex.getMessage();
		if (ex.getClass() == JpaObjectRetrievalFailureException.class) {
			errorStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return ResponseEntity.status(errorStatus).body(message);
	}
}
