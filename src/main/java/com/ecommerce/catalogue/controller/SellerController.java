package com.ecommerce.catalogue.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.catalogue.model.Seller;
import com.ecommerce.catalogue.service.ProductService;
import com.ecommerce.catalogue.service.SellerService;

@RestController
@RequestMapping("/api/v1")
public class SellerController {
	@Autowired
	private SellerService sellerService;

	@Autowired
	private ProductService productService;

	@GetMapping("/seller/products/count/{id}")
	public @ResponseBody Integer getCountOfProducts(@PathVariable Long id) {
		return this.productService.getNoOfProductBySeller(id);
	}

	@PostMapping("/seller")
	public @ResponseBody Seller postSeller(@Valid @RequestBody Seller seller) {
		return this.sellerService.save(seller);
	}
}
