package com.ecommerce.catalogue.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.catalogue.model.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
	Product findByName(String name);

	Optional<Product> findById(Long id);

	Page<Product> findAll(Pageable pageable);

	Integer countBySellerId(Long id);

	Page<Product> findByPrice(float price, Pageable pageable);

	Page<Product> findByColor(String color, Pageable pageable);

	Page<Product> findBySize(String color, Pageable pageable);

	Page<Product> findByBrand(Long id, Pageable pageable);
}
