package com.ecommerce.catalogue.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.catalogue.model.Category;

@Service
public interface CategoryService {
	Category save(Category category);

	Category update(Category category, Long id);

	List<Category> findAll(int page, int pageSize, int sortOrder);
}
