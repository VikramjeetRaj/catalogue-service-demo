package com.ecommerce.catalogue.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.catalogue.dto.CategoryDTO;
import com.ecommerce.catalogue.model.Category;
import com.ecommerce.catalogue.service.CategoryService;

@RestController
@RequestMapping("api/v1")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@GetMapping("/category")
	public List<Category> getAllCategories(@RequestParam Integer page, @RequestParam Integer pageSize,
			@RequestParam Integer sortOrder) {
		return this.categoryService.findAll(page, pageSize, sortOrder);
	}

	@PostMapping("/category")
	public Category newCategory(@Valid @RequestBody CategoryDTO category) {
		Category categoryEntity = new Category();
		categoryEntity.setName(category.getName());
		return this.categoryService.save(categoryEntity);
	}

	@PutMapping("/category/{id}")
	public Category replaceCategory(@Valid @RequestBody CategoryDTO category, @PathVariable Long id) {
		Category categoryEntity = new Category();
		categoryEntity.setName(category.getName());
		return this.categoryService.update(categoryEntity, id);
	}
}
