package com.ecommerce.catalogue.dto;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class ProductDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	private String name;

	@NotNull
	private float price;

	@NotNull
	private String color;

	@NotNull
	private float size;

	@NotNull
	private Long brandId;

	@NotNull
	private Long sellerId;

	@NotNull
	private Set<Long> categories;

	public ProductDTO() {
	}

	public ProductDTO(@NotNull String name, @NotNull float price, @NotNull String color, @NotNull float size,
			@NotNull Long brandId, @NotNull Long sellerId, @NotNull Set<Long> categories) {
		this.name = name;
		this.price = price;
		this.color = color;
		this.size = size;
		this.brandId = brandId;
		this.sellerId = sellerId;
		this.categories = categories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public Set<Long> getCategories() {
		return categories;
	}

	public void setCategories(Set<Long> categories) {
		this.categories = categories;
	}

	@Override
	public String toString() {
		return "ProductDTO [name=" + name + ", price=" + price + ", color=" + color + ", size=" + size + ", brandId="
				+ brandId + ", sellerId=" + sellerId + ", categories=" + categories + "]";
	}

}
