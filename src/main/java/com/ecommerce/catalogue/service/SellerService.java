package com.ecommerce.catalogue.service;

import com.ecommerce.catalogue.model.Seller;

public interface SellerService {
	Seller save(Seller seller);

	Seller update(Seller seller, Long id);
}
