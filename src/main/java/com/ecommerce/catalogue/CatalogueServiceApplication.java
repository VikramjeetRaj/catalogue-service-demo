package com.ecommerce.catalogue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableCaching
public class CatalogueServiceApplication {
	
	@Autowired
    private CacheManager cacheManager;
	
	public static void main(String[] args) {
		SpringApplication.run(CatalogueServiceApplication.class, args);
	}
	
	@Scheduled(fixedRate = 2 * 60 * 60 * 1000)
	public void evictAllcachesAtIntervals() {
		this.cacheManager.getCache("products").clear();
	}

}
