package com.ecommerce.catalogue.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.catalogue.dto.ProductDTO;
import com.ecommerce.catalogue.model.Brand;
import com.ecommerce.catalogue.model.Category;
import com.ecommerce.catalogue.model.Product;
import com.ecommerce.catalogue.model.Seller;
import com.ecommerce.catalogue.service.ProductService;

@RestController
@RequestMapping("api/v1")
public class ProductController {
	@Autowired
	private ProductService productService;

	@GetMapping("/product")
	@Cacheable(value = "products")
	public @ResponseBody List<Product> getProductBy(@RequestParam String orderByField, @RequestParam Integer page,
			@RequestParam Integer pageSize, @RequestParam Integer sortOrder) {
		return this.productService.findAllProductGroupBy(orderByField, page, pageSize, sortOrder);
	}

	@CacheEvict(value = "products", allEntries = true)
	@PostMapping("/product")
	public @ResponseBody Product newProduct(@Valid @RequestBody ProductDTO product) {
		Seller seller = new Seller();
		seller.setId(product.getSellerId());

		Brand brand = new Brand();
		brand.setId(product.getBrandId());

		Set<Category> categories = new HashSet<>();
		for (Long id : product.getCategories()) {
			Category category = new Category();
			category.setId(id);
			categories.add(category);
		}
		Product productEntity = new Product(product.getName(), product.getPrice(), product.getColor(),
				product.getSize(), seller, brand, categories);
		return this.productService.save(productEntity);
	}

	@CacheEvict(value = "products", allEntries = true)
	@PutMapping("/product/{id}")
	public @ResponseBody Product replaceProduct(@Valid @RequestBody ProductDTO product, @PathVariable Long id) {
		Seller seller = new Seller();
		seller.setId(product.getSellerId());

		Brand brand = new Brand();
		brand.setId(product.getBrandId());

		Set<Category> categories = new HashSet<>();
		for (Long categoryId : product.getCategories()) {
			Category category = new Category();
			category.setId(categoryId);
			categories.add(category);
		}
		Product productEntity = new Product(product.getName(), product.getPrice(), product.getColor(),
				product.getSize(), seller, brand, categories);
		return this.productService.update(productEntity, id);
	}
}
