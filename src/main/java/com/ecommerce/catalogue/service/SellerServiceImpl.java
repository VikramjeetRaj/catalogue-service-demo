package com.ecommerce.catalogue.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.catalogue.model.Seller;
import com.ecommerce.catalogue.repository.SellerRepository;

@Service
public class SellerServiceImpl implements SellerService {

	@Autowired
	private SellerRepository sellerRepository;

	public Seller save(Seller seller) {
		return this.sellerRepository.save(seller);
	}

	public Seller update(Seller inputSeller, Long id) {
		return this.sellerRepository.findById(id).map(seller -> {
			seller.setAccountId(inputSeller.getAccountId());
			return this.sellerRepository.save(seller);
		}).orElseGet(() -> {
			inputSeller.setId(id);
			return this.sellerRepository.save(inputSeller);
		});
	}

}
