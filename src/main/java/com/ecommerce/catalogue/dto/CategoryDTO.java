package com.ecommerce.catalogue.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class CategoryDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotNull
	private String name;

	public CategoryDTO(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CategoryDTO [name=" + name + "]";
	}

}
