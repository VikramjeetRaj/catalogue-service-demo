package com.ecommerce.catalogue.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.catalogue.model.Category;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
	Page<Category> findAll(Pageable pageable);

	Optional<Category> findByName(String name);
}
