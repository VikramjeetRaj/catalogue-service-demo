package com.ecommerce.catalogue.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.catalogue.model.Brand;

@Repository
public interface BrandRepository extends PagingAndSortingRepository<Brand, Long> {
	Optional<Brand> findByName(String name);

	Page<Brand> findAll(Pageable pageable);
}
