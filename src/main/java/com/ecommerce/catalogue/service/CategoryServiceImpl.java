package com.ecommerce.catalogue.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ecommerce.catalogue.model.Category;
import com.ecommerce.catalogue.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	public Category save(Category category) {
		return this.categoryRepository.save(category);
	}

	public Category update(Category inputCategory, Long id) {
		return categoryRepository.findById(id).map(category -> {
			category.setName(inputCategory.getName());
			return categoryRepository.save(category);
		}).orElseGet(() -> {
			inputCategory.setId(id);
			return categoryRepository.save(inputCategory);
		});
	}

	public List<Category> findAll(int page, int pageSize, int sortOrder) {
		Sort.Direction direction;

		if (sortOrder == 0) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}
		return this.categoryRepository.findAll(PageRequest.of(page, pageSize, direction, "name")).toList();
	}

}
