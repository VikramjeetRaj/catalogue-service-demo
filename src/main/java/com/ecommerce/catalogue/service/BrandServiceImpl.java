package com.ecommerce.catalogue.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ecommerce.catalogue.model.Brand;
import com.ecommerce.catalogue.repository.BrandRepository;

@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandRepository brandRepository;

	public Brand save(Brand brand) {
		return this.brandRepository.save(brand);
	}

	public Brand update(Brand inputBrand, Long id) {
		return this.brandRepository.findById(id).map(brand -> {
			brand.setName(inputBrand.getName());
			return this.brandRepository.save(brand);
		}).orElseGet(() -> {
			inputBrand.setId(id);
			return this.brandRepository.save(inputBrand);
		});
	}

	public List<Brand> findAll(int page, int pageSize, int sortOrder) {
		Sort.Direction direction;

		if (sortOrder == 0) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		return this.brandRepository.findAll(PageRequest.of(page, pageSize, direction, "name")).toList();
	}

}
