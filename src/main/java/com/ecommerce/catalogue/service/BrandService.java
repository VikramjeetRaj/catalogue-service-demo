package com.ecommerce.catalogue.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.catalogue.model.Brand;

@Service
public interface BrandService {
	Brand save(Brand brand);

	Brand update(Brand brand, Long id);

	List<Brand> findAll(int page, int pageSize, int sortOrder);
}
