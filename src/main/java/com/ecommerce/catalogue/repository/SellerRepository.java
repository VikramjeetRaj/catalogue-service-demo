package com.ecommerce.catalogue.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.catalogue.model.Seller;

@Repository
public interface SellerRepository extends JpaRepository<Seller, Long> {
}
